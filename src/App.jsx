import { useState, useEffect } from 'react'
import './App.css'
import {fsljs} from '@fmrib/fsljs'
import { 
  AppContainer, 
  Title, 
  FileChooser, 
  Row, 
  CommandPreview, 
} from '@fmrib/widgets'
import Button from '@mui/material/Button';

export default function App() {

  const [running, setRunning] = useState(false)
  const [opts, setOpts] = useState(fsljs.defaultInvertXFMOpts)
  const [commandString, setCommandString] = useState('')

  // watch for changes to the JSON string version of opts
  useEffect(() => {
    // create the command string for this tool (sends a message to the main process)
    async function createCommandString(){
      let command = 'convert_xfm'
      let commandString = await fsljs.createCommandString({command, opts})
      setCommandString(commandString)
    }
    autoSetOutputFile()
    createCommandString()
  }, [JSON.stringify(opts)])

  async function onRunClick(){
    setRunning(true)
    let output = await fsljs.run({
      command: 'convert_xfm',
      opts: opts
    })
    console.log('from main', output)
    setRunning(false)
  }

  async function onInputFileClick(){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the '-in' option
    setOpts({...opts, 'in': inputFile})
  }

  function onOutputFileChange(value){
    setOpts({...opts, '-omat': value})
  }

  function autoSetOutputFile(){
    let inputFile = opts['in']
    // do nothing if either inputFile or refFile are empty strings
    if (inputFile === ''|| inputFile === null) return
    let inputDirname = fsljs.dirname(inputFile)
    let inputBasename = fsljs.removeExtension(fsljs.basename(inputFile), '.mat')
    let outputFile = fsljs.join(inputDirname, `inverse_${inputBasename}.mat`)
    setOpts({...opts, '-omat': outputFile})
  }

  return (
    <AppContainer gap={2}>
      <Title>
        Invert XFM
      </Title>
      <FileChooser value={opts['in']} textLabel="Transformation matrix (A to B) .mat file" onClick={onInputFileClick}/>
      <FileChooser value={opts['-omat']} textLabel="Output (inverse B to A)" onChange={onOutputFileChange}/>
        {/* command string */}
      <CommandPreview commandString={commandString} multiLine={true}/>
        <Row gap={2} marginTop={2}>
          {/* Run button */}
          <Button variant='contained' onClick={onRunClick} disabled={running}>Run</Button>
        </Row>
    </AppContainer>
  )
}
